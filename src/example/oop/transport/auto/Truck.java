package example.oop.transport.auto;
import example.oop.transport.interfaces.CargoAuto;

public class Truck implements CargoAuto {
    @Override
    public void transportCargo() {
        System.out.println("Везёт груз");
    }
}
