package example.oop.transport.auto;

public class Demo {
    public static void main(String[] args){
        Truck kamaz  = new Truck();
        Taxi audi  =  new Taxi();
        Minivan microbus = new Minivan();

        kamaz.transportCargo();
        audi.transportPassangers();
        microbus.transportCargo();
        microbus.transportPassangers();
    }
}
