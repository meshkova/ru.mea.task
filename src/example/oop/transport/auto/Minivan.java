package example.oop.transport.auto;
import example.oop.transport.interfaces.CargoAuto;
import example.oop.transport.interfaces.PassangersAuto;

public class Minivan implements PassangersAuto, CargoAuto {

    @Override
    public void transportCargo() {
        System.out.println("Везёт груз");
    }

    @Override
    public void transportPassangers() {
        System.out.println("Везёт пассажиров");
    }
}
