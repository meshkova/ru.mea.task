package example.oop.transport.interfaces;

public interface CargoAuto {
    void transportCargo();
}
