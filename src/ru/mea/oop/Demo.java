package ru.mea.oop;
import java.util.Scanner;
public class Demo {
    private static Scanner scanner = new Scanner(System.in);


    public static void main(String[] args) {
        String yes = "да";
        String no = "нет";
        String answer;

        do {
            Tree tree3 = input();
            if (tree3.isIsboolean() == true) {
                System.out.println(Tree.trees(tree3));
            } else {
                System.out.println(Tree.ftrees(tree3));

            }
            System.out.println("Вам подходит эта ёлка?");
            scanner.nextLine();
            answer = scanner.nextLine();
        }   while (!answer.equals(yes));
    }

    public static Tree input() {

        System.out.println("Введите  какую ёлку вы хотите: настоящую - true; искуственную - false;");
        boolean isboolean;
        isboolean = scanner.nextBoolean();
        System.out.println("Введите длину");
        int sais = scanner.nextInt();
        return new Tree(isboolean, sais);
    }
}
