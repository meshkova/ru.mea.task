package ru.mea.oop;

public class Tree {
    protected boolean isboolean;
    private int sais;

    public Tree(boolean isboolean, int sais) {
        this.isboolean = isboolean;
        this.sais = sais;
    }

    public boolean isIsboolean() {
        return isboolean;
    }

    public int getSais() {
        return sais;
    }

    @Override
    public String toString() {
        return "Tree{" +
                "isboolean=" + isboolean +
                ", sais=" + sais +
                '}';
    }

    public static int trees(Tree tree3) {

        int money = 800;

        if (tree3.isIsboolean() == true && tree3.getSais() < 100) {
            for (int i = 0; i < tree3.getSais(); i++)
                money--;
        }
        if (tree3.isIsboolean() == true && tree3.getSais() > 100) {
            for (int i = 0; i < tree3.getSais(); i++)
                money++;


        }
        return money;
    }

    static int ftrees(Tree tree3) {
        int money = 600;
        if (tree3.isIsboolean() == false && tree3.getSais() > 100) {
            for (int i = 0; i < tree3.getSais(); i++)
                money++;
        }
        if (tree3.isIsboolean() == false && tree3.getSais() < 100) {
            for (int i = 0; i < tree3.getSais(); i++)
                money--;


        }
        return money;
    }
}
