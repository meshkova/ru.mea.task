package ru.mea.pizza;

public class Pizza {
    private String name;
    private double diameter;
    private double money;

    public Pizza(String name, double diameter, double money) {
        this.name = name;
        this.diameter = diameter;
        this.money = money;
    }

    public Pizza() {
        this("Маргарита", 25, 350);
    }

    public String getName() {
        return name;
    }

    public double getDiameter() {
        return diameter;
    }

    public double getMoney() {
        return money;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "name='" + name + '\'' +
                ", diameter=" + diameter +
                ", money=" + money +
                '}';
    }

     double piz() {
        double area = 0;
        double price = 0;
        double pi = 3.14;

        area = pi * Math.pow((getDiameter() / 2), 2);
        price = money/ area;

        return price;
    }

}
